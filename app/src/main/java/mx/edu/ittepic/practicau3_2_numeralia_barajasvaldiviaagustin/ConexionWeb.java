package mx.edu.ittepic.practicau3_2_numeralia_barajasvaldiviaagustin;

import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by agustin on 29/03/18.
 */

public class ConexionWeb extends AsyncTask<URL, String, String>{

    List<String[]> variables;
    MainActivity puntero;
    String respuesta;

    public ConexionWeb(MainActivity p){
        puntero = p;
        variables = new ArrayList<String[]>();
    }
    public void agregarVariable(String nombre, String contenido){
        //Formar la lista de clave : valor en un arrglo por posición
        String[] temporal = {nombre, contenido};
        variables.add(temporal);
    }
    @Override
    protected String doInBackground(URL... urls) {
        //Generar cadena de envío
        String POST="";
        respuesta = "";
        float mayor = 0;
        float menor = 100;
        //Formar la cadena de parámetros a enviar al servidor
        for (String[] var: variables){
            try{
                POST += var[0]+"="+ URLEncoder.encode(var[1], "UTF-8")+" ";
            }catch(Exception e){
                return "ERROR_404_0";
            }
        }
        POST = POST.trim();
        POST = POST.replace(" ", "&");

        //Conexión con el servidor
        HttpURLConnection conexion = null;

        try{
            publishProgress("Conectando");
            conexion = (HttpURLConnection)urls[0].openConnection();
            conexion.setDoInput(true);
            conexion.setFixedLengthStreamingMode(POST.length());//Indicar la cantidad de bytes que se van a enviar
            conexion.setRequestMethod("POST");
            conexion.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            OutputStream flujoSalida = new BufferedOutputStream(conexion.getOutputStream());
            flujoSalida.write(POST.getBytes());
            flujoSalida.flush();
            flujoSalida.close();

            if (conexion.getResponseCode() == 200) {
                InputStreamReader input = new InputStreamReader(conexion.getInputStream(), "UTF-8");
                BufferedReader flujoEntrada = new BufferedReader(input);
                String linea = "";
                do{
                    linea = flujoEntrada.readLine();
                    if (linea!=null){
                        respuesta += linea;
                    }
                }while(linea != null);
                flujoEntrada.close();
                //Procesar los números recibidos de la app web para obtener el mayor y el menor
                String[] numbers = respuesta.split(",");

                for (String number : numbers){
                    float numb = Float.parseFloat(number);
                    if (numb > mayor){
                        mayor = numb;
                    }
                    if (numb < menor){
                        menor = numb;
                    }
                }

            }else {
                return "ERROR_404_1";
            }
        }catch (UnknownHostException e){
            //NO se encontró el servidor
            return "ERROR_404_2";
        }catch (IOException er) {
            //NO se pueden enviar o recibir datos
            return "ERROR_404_1";
        }finally {
            if (conexion != null){
                conexion.disconnect();
            }
        }
        return mayor+"&"+menor;
    }
    @Override
    protected void onProgressUpdate(String... r){
        puntero.cambiarMensaje(r[0]);
    }
    @Override
    protected void onPostExecute(String respuesta){
        if (!respuesta.contains("&")){
            puntero.procesarRespuesta(respuesta);
            return;
        }
        String[] cad = respuesta.split("&");
        puntero.procesarRespuesta(new float[]{Float.parseFloat(cad[0]),Float.parseFloat(cad[1])});
    }
}
