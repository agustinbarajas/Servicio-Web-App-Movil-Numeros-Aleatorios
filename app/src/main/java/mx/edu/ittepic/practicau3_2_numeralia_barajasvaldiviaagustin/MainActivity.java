package mx.edu.ittepic.practicau3_2_numeralia_barajasvaldiviaagustin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    EditText numeros;
    TextView may, men;
    Button generar, numgen;
    ConexionWeb conexionweb;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numeros = findViewById(R.id.numeros);
        generar = findViewById(R.id.generar);
        may = findViewById(R.id.mayor);
        men = findViewById(R.id.menor);
        numgen = findViewById(R.id.numgen);
        numgen.setEnabled(false);

        generar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                try {
                    conexionweb = new ConexionWeb(MainActivity.this);
                    conexionweb.agregarVariable("num", (numeros.getText().toString().isEmpty())?5+"":numeros.getText().toString());
                    URL direccion = new URL("https://tpdmagustin.000webhostapp.com/generador.php");
                    dialog = ProgressDialog.show(MainActivity.this, "Atención", "Conectando");
                    conexionweb.execute(direccion);
                    numgen.setEnabled(true);
                }catch(MalformedURLException e){
                    Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_LONG).show();
                }
            }
        });

        numgen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                String cad = "";
                String[] nums = conexionweb.respuesta.split(",");
                for(int pos= 0 ; pos < nums.length ; pos++){
                    cad += "(" + (pos+1) + ") " + nums[pos] + "\n";
                }
                alert.setTitle("Números generados").setMessage(cad);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });
    }
    public void procesarRespuesta(float[] maymen) {
        dialog.dismiss();
        may.setText(maymen[0] + "");
        men.setText(maymen[1] + "");
    }
    public void procesarRespuesta(String respuesta) {
        AlertDialog.Builder alert =  new AlertDialog.Builder(this);
        dialog.dismiss();
        if (respuesta.equals("ERROR_404_1")){
            respuesta = "ERROR: Flujo de Entrada/Salida no funcionó";
        }
        if (respuesta.equals("ERROR_404_2")){
            respuesta = "ERROR: Servidor caido o dirección incorrecta";
        }
        alert.setTitle("Advertencia").setMessage(respuesta);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }
    public void cambiarMensaje(String s) {
        dialog.setMessage(s);
    }
}
